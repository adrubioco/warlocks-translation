# Warlocks board game translation

## Roadmap
  - [X] English (obviously done)
  - [ ] Spanish (WIP)
  - [ ] Catalan (someday)

## Origin of the game and interesting links
   - [https://mud.co.uk/richard/spellbnd.htm](Author of the original pen & paper game)
   - [https://games.ravenblack.net/rules/1/intro.html](Web game)
   
## Credits
   - Richard A. Bartle, creator of the original game.
   - Raven Black, creator and maintainer of the web version.
